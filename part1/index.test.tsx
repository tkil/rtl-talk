import React from "react"
import { render, waitFor } from "@testing-library/react"

import { Buttonstan } from "./index"

describe("Component: Button Land", () => {
  it.skip("debug", () => {
    const { debug } = render(<Buttonstan />)
    debug()
  })

  it.skip("bad get", () => {
    const { getByRole } = render(<Buttonstan />)
    getByRole("button", { name: "Imaginary Button" })
  })

  it("should have a boring button", () => {
    const { queryByRole, getByRole } = render(<Buttonstan />)
    // queryByRole yields -> HTML | null
    expect(queryByRole("button", { name: "Boring Button" })).toBeInTheDocument()
  })

  it("should not have a lazy button right away", () => {
    const { queryByRole } = render(<Buttonstan />)
    expect(queryByRole("button", { name: "Lazy Button" })).not.toBeInTheDocument()
  })

  it("should have a red boring button", () => {
    const { getByRole } = render(<Buttonstan />)
    expect(getByRole("button", { name: /boring button/i })).toHaveAttribute("data-color", "red")
    expect(getByRole("button", { name: /Boring/ })).toHaveAttribute("data-color", "red")
  })

  it("should have a blue music button", () => {
    const { getByRole } = render(<Buttonstan />)
    expect(getByRole("button", { name: "Music Button" })).toHaveAttribute("data-color", "blue")
  })

  it("should have black composed button by default", () => {
    const { getByRole } = render(<Buttonstan />)
    expect(getByRole("button", { name: "Composed Button" })).toHaveAttribute("data-color", "black")
  })

  it.each([
    // Parameterized Test
    "yellow",
    "green",
    "black",
    "teal",
  ])("should have %s composed button", (composedColor) => {
    const { getByRole } = render(<Buttonstan composedColor={composedColor} />)
    // console.log(getByRole("button", { name: "Composed Button" }).outerHTML)
    expect(getByRole("button", { name: "Composed Button" })).toHaveAttribute("data-color", composedColor)
  })

  it("should have a lazy green button that appears in a moment", async () => {
    const { queryByRole, findByRole } = render(<Buttonstan />)
    // Initially missing
    expect(queryByRole("button", { name: "Lazy Button" })).not.toBeInTheDocument()
    // Appears in a moment
    expect(await findByRole("button", { name: "Lazy Button" })).toBeInTheDocument()
    expect(await findByRole("button", { name: "Lazy Button" })).toHaveAttribute("data-color", "green")
  })

  it("should have tricky button change from purple to orange", async () => {
    const { getByRole } = render(<Buttonstan />)

    expect(getByRole("button", { name: "Tricky Button" })).toHaveAttribute("data-color", "purple")
    await waitFor(() => {
      // console.log(getByRole("button", { name: "Tricky Button" }).outerHTML)
      expect(getByRole("button", { name: "Tricky Button" })).toHaveAttribute("data-color", "orange")
    })
  })
})
