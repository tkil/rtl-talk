import React from "react"

export interface IMyComponent {
  composedColor?: string
}

export const Buttonstan: React.FC<IMyComponent> = ({ composedColor = "black" }) => {
  const [isDelayedShown, setIsDelayedShown] = React.useState<boolean>(false)
  const [multiColor, setMultiColor] = React.useState<string>("purple")

  React.useEffect(() => {
    const timeouts = [
      setTimeout(() => {
        setIsDelayedShown(true)
      }, 200),

      setTimeout(() => {
        setMultiColor("orange")
      }, 400),
    ]

    // Cleanup
    return () => {
      timeouts.forEach((to) => clearTimeout(to))
    }
  }, [])

  return (
    <div className="buttonstan">
      <h1>Buttonstan</h1>
      <h2>Land of the Buttons!</h2>

      <main aria-label="button land">
        <button type="button" data-color="red">
          Boring Button
        </button>
        <button type="button" aria-label="Music Button" data-color="blue">
          Yomama
        </button>
        <button type="button" data-color={composedColor}>
          Composed Button
        </button>
        {isDelayedShown && (
          <button type="button" data-color="green">
            Lazy Button
          </button>
        )}
        <button type="button" data-color={multiColor}>
          Tricky Button
        </button>

        <div>
          <button type="button">
            Some Button
          </button>
        
          <button type="button"
            aria-label="Click Me"
          >▶️</button>
        
          <button type="button">
            Other Button
          </button>
        </div>


      </main>
    </div>
  )
}

export default Buttonstan
