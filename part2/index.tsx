import React from "react"

interface ISubmission {
  name: string
  dishName: string
  dishType: string
  dishDescription: string
  allergens: string[]
}

export interface IFoodie {
  onSubmit: (submission: ISubmission) => void
}

export const Foodie: React.FC<IFoodie> = ({ onSubmit }) => {
  const [yourName, setYourName] = React.useState("")
  const [dishName, setDishName] = React.useState("")
  const [dishType, setDishType] = React.useState("")
  const [errorMessage, setErrorMessage] = React.useState("")
  const [isShowDescription, setIsShowDescription] = React.useState(false)
  const [dishDescription, setDishDescription] = React.useState("")
  const [allergenPeanut, setAllergenPeanut] = React.useState(false)
  const [allergenTreeNut, setAllergenTreeNut] = React.useState(false)
  const [allergenEgg, setAllergenEgg] = React.useState(false)
  const [allergenSoy, setAllergenSoy] = React.useState(false)

  const handleOnDishTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setDishType(e.target.value)
    setIsShowDescription(true)
  }

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const errors = []
    const allergens = []
    if (allergenPeanut) {
      allergens.push("peanut")
    }
    if (allergenTreeNut) {
      allergens.push("peanut")
    }
    if (allergenEgg) {
      allergens.push("egg")
    }
    if (allergenSoy) {
      allergens.push("soy")
    }

    if (!yourName) {
      errors.push("Your name is required")
    }

    if (!dishName) {
      errors.push("Dish name is required")
    }

    if (!dishType) {
      errors.push("Dish type is required")
    }

    if (errors.length > 0) {
      setErrorMessage(errors.join("\n"))
    } else {
      onSubmit({
        name: yourName,
        dishName,
        dishType,
        dishDescription,
        allergens,
      })
    }
  }

  return (
    <div className="foodie">
      <h1>Potluck Sign Up</h1>
      <main aria-label="foods of the world">
        <form onSubmit={handleSubmit} aria-label="sign up form for a dish to pass">
          {/* Your Name */}
          <label htmlFor="name">Your Name</label>
          <input type="text" id="name" name="Name" value={yourName} onChange={(e) => setYourName(e.target.value)} />

          {/* Dish Name */}
          <label htmlFor="dish-name">Dish Name</label>
          <input type="text" id="dish-name" name="Dish Name" value={dishName} onChange={(e) => setDishName(e.target.value)} />

          {/* Dish Type */}
          <label htmlFor="dish-type">Dish Type</label>
          <select id="dish-type" name="type" value={dishType} onChange={handleOnDishTypeChange}>
            <option value="">--Select Your Dish Type</option>
            <option value="main">Main</option>
            <option value="side">Side</option>
            <option value="dessert">Dessert</option>
            <option value="drink">Drink</option>
          </select>

          <select>
            <option>Main</option>
            <option>Side</option>
            <option>Dessert</option>
            <option>Drink</option>
          </select>

          {/* Dish Description */}
          {isShowDescription && (
            <>
              <label htmlFor="describe-dish">Describe your dish</label>
              <textarea id="describe-dish" name="describe" rows={6} cols={32} value={dishDescription} onChange={(e) => setDishDescription(e.target.value)} />
            </>
          )}

          {/* Allergens */}
          <h2>Allergens</h2>
          <label htmlFor="allergens-1">Peanut</label>
          <input type="checkbox" id="allergens-1" name="Peanut" checked={allergenPeanut} onChange={(e) => setAllergenPeanut(e.target.checked)} />
          <label htmlFor="allergens-2">Tree Nut</label>
          <input type="checkbox" id="allergens-2" name="Tree Nut" checked={allergenTreeNut} onChange={(e) => setAllergenTreeNut(e.target.checked)} />
          <label htmlFor="allergens-3">Egg</label>
          <input type="checkbox" id="allergens-3" name="Egg" checked={allergenEgg} onChange={(e) => setAllergenEgg(e.target.checked)} />
          <label htmlFor="allergens-4">Soy</label>
          <input type="checkbox" id="allergens-4" name="Soy" checked={allergenSoy} onChange={(e) => setAllergenSoy(e.target.checked)} />

          {errorMessage && (
            <p className="errors" role="alert" aria-label="error messages">
              {errorMessage}
            </p>
          )}

          <button type="submit">Submit</button>
        </form>
      </main>
    </div>
  )
}

export default Foodie
