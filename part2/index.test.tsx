import React from "react"
import { fireEvent, render, waitFor } from "@testing-library/react"
import userEventLibrary from "@testing-library/user-event"
import { UserEvent } from "@testing-library/user-event/dist/types/setup"

import { Foodie, IFoodie } from "./index"

describe("Component: Foodie", () => {
  let mockProps: IFoodie
  let userEvent: UserEvent
  beforeEach(() => {
    mockProps = {
      onSubmit: () => {},
    }
    userEvent = userEventLibrary.setup()
  })

  it.skip("debug", () => {
    const { debug } = render(<Foodie {...mockProps} />)
    debug()
  })


  it("should enter their name", async () => {
    // Arrange
    const { getByLabelText } = render(<Foodie {...mockProps} />)

    // Act
    // fireEvent.change(getByLabelText("Your Name"), { target: { value: "Ray Finkle" } })
    await userEvent.type(getByLabelText("Your Name"), "Ray Finkle")

    // Assert
    await waitFor(() => {
      expect(getByLabelText("Your Name")).toHaveValue("Ray Finkle")
    })
  })

  it("should enter dish name", async () => {
    // Arrange
    const { getByLabelText } = render(<Foodie {...mockProps} />)

    // Act
    fireEvent.change(getByLabelText("Dish Name"), { target: { value: "Pizza" } })
    // await userEvent.type(getByLabelText("Dish Name"), "Pizza")

    // Assert
    await waitFor(() => {
      expect(getByLabelText("Dish Name")).toHaveValue("Pizza")
    })
  })

  it("should select peanut allergy", async () => {
    // Arrange
    const { getByLabelText } = render(<Foodie {...mockProps} />)

    // Act
    await userEvent.click(getByLabelText("Peanut"))

    // Assert
    await waitFor(() => {
      expect(getByLabelText("Peanut")).toBeChecked()
    })
  })

  it("should select a main dish type", async () => {
    // Arrange
    const { getByLabelText } = render(<Foodie {...mockProps} />)

    // Act
    await userEvent.selectOptions(getByLabelText("Dish Type"), "Main")

    // Assert
    await waitFor(() => {
      expect(getByLabelText("Dish Type")).toHaveValue("main")
    })
  })

  it("should not show describe your dish until type is selected", async () => {
    // Arrange
    const { getByLabelText, queryByLabelText } = render(<Foodie {...mockProps} />)

    expect(queryByLabelText("Describe your dish")).not.toBeInTheDocument()

    // Act
    await userEvent.selectOptions(getByLabelText("Dish Type"), "Main")

    // Assert
    await waitFor(() => {
      expect(queryByLabelText("Describe your dish")).toBeInTheDocument()
    })
  })

  it("should show error on empty submission", async () => {
    // Arrange
    mockProps.onSubmit = jest.fn()
    const { getByRole, queryByRole } = render(<Foodie {...mockProps} />)

    // Act
    await userEvent.click(getByRole("button", { name: "Submit" }))

    // Assert
    await waitFor(() => {
      expect(queryByRole("alert", { name: "error messages" })).toBeInTheDocument()
      expect(queryByRole("alert", { name: "error messages" })).toHaveTextContent(/your name is required/i)
      expect(mockProps.onSubmit).not.toBeCalled()
    })
  })

  it("should show dish type requirement", async () => {
    // Arrange
    mockProps.onSubmit = jest.fn()
    const { getByLabelText, getByRole, queryByRole } = render(<Foodie {...mockProps} />)

    // Act
    await userEvent.type(getByLabelText("Your Name"), "Ray Finkle")
    await userEvent.click(getByRole("button", { name: "Submit" }))

    // Assert
    await waitFor(() => {
      expect(queryByRole("alert", { name: "error messages" })).toBeInTheDocument()
      expect(queryByRole("alert", { name: "error messages" })).toHaveTextContent(/dish type is required/i)
      expect(mockProps.onSubmit).not.toBeCalled()
    })
  })

  it("should submit a dish entry", async () => {
    // Arrange
    mockProps.onSubmit = jest.fn()
    const { getByLabelText, getByRole, queryByRole } = render(<Foodie {...mockProps} />)

    // Act
    await userEvent.type(getByLabelText("Your Name"), "Ray Finkle")
    await userEvent.type(getByLabelText("Dish Name"), "Pizza")
    await userEvent.selectOptions(getByLabelText("Dish Type"), "Main")
    await userEvent.click(getByRole("button", { name: "Submit" }))

    // Assert
    await waitFor(() => {
      expect(queryByRole("alert", { name: "error messages" })).not.toBeInTheDocument()
      expect(mockProps.onSubmit).toBeCalledWith({
        allergens: [],
        dishDescription: "",
        dishName: "Pizza",
        dishType: "main",
        name: "Ray Finkle",
      })
    })
  })
})
