module.exports = {
  testEnvironment: "jsdom",
  clearMocks: true,
  collectCoverage: false,
  collectCoverageFrom: [
    // Match Pattern
    "**/*.{js,jsx,ts,tsx}",
    // Ignore Directories
    "!**.expected.**",
    "!**/__tests__/**",
    "!**/assets/**",
    "!**/bin/**",
    "!**/cli/**",
    "!**/coverage/**",
    "!**/dist/**",
    "!**/env/**",
    "!**/node_modules/**",
    "!**/tests/**",
    "!**/types/**",
    "!**/vendor/**",
    // Ignore Files
    "!**/**.config.**",
  ],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: ["/cli/", "/node_modules/"],
  moduleFileExtensions: ["js", "jsx", "ts", "tsx", "vue", "json"],
  setupFilesAfterEnv: ["<rootDir>/jest.setup.js"],
  moduleNameMapper: {
    ".*.(css|scss)": "<rootDir>/src/testing/stubs/nullStub.js",
    "^@/(.*)$": "<rootDir>/src/$1",
    "@env/(.*)": "<rootDir>/env/$1",
    "@modules/(.*)": "<rootDir>/node_modules/$1",
  },
  modulePathIgnorePatterns: [],
  testMatch: [
    "**/index.test.(js|jsx|ts|tsx)",
    "**/__tests__/*test.+(js|jsx|ts|tsx)",
    "**/tests/integration/*test.+(js|jsx|ts|tsx)",
  ],
  testPathIgnorePatterns: ["/node_modules/"],
  transformIgnorePatterns: ["<rootDir>/node_modules/"],
  transform: {
    "\\.(ts|js)x?$": "ts-jest",
    "^.+\\.jsx?$": "babel-jest",
  },
}
